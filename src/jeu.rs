//! Implémente les mécaniques du jeu

#[derive(Debug)]
/// Represente une partie de morpion
pub struct Partie {
    /// L'état de la partie
    grille: [[u8; 3]; 3],
    /// Le nombre de coup joués
    tour: u8,
}

impl Partie {
    /// Renvoie une partie vide
    pub fn new() -> Partie {
        Partie {
            grille: [[0; 3]; 3],
            tour: 0,
        }
    }
    /// Génere la représentation de l'état de la partie sous forme d'une chaine
    pub fn to_string(&self) -> String {
        let mut representation = String::new();
        for idx_ligne in 0..3 {
            for idx_colonne in 0..3 {
                match self.grille[idx_ligne][idx_colonne] {
                    0 => representation.push(' '),
                    1 => representation.push('X'),
                    2 => representation.push('O'),
                    _ => panic!("Valeur inatendu"),
                };
                if idx_colonne < 2 {
                    representation.push('|');
                } else {
                    representation.push('\n');
                }
            }
            if idx_ligne < 2 {
                representation.push_str("-+-+-\n");
            }
        }
        representation
    }
    /// Joue un coup dans la grille
    ///
    /// # Argument
    /// * ```ligne``` : la ligne dans la quelle on joue, entre 0 et 2
    /// * ```colonne``` : la colonne dans la quelle on joue, entre 0 et 2
    ///
    /// # Result
    /// Soit :
    /// * ```Ok(tour)``` avec ```tour``` le numéro du tour qui viens d'avoir lieu
    /// * ```Err(joueur)``` avec ```joueur``` le numéro du joueur déjà présent dans la case
    ///
    /// # Panic
    /// Si la position est en dehors du plateau, la fonction panic.
    pub fn jouer(&mut self, ligne: usize, colonne: usize) -> Result<u8, u8> {
        if (ligne > 2) | (colonne > 2) {
            panic!("On joue trop loin");
        }
        if self.grille[ligne][colonne] == 0 {
            self.grille[ligne][colonne] = self.joueur_a_jouer();
            self.tour += 1;
            return Ok(self.tour);
        } else {
            return Err(self.grille[ligne][colonne]);
        }
    }
    /// Renvoie le numéro du joueur devant jouer
    pub fn joueur_a_jouer(&self) -> u8 {
        return (self.tour % 2) + 1;
    }
    /// Renvoie le joueur vainceur si il y en a un
    /// # Result
    /// * ```Some(joueur)``` avec ```joueur``` le numéro du joueur vainceur
    /// * ```None``` si personne ne gagne
    pub fn victoire(&self) -> Option<u8> {
        for idx_1 in 0..3 {
            // Teste des lignes
            let joueur = self.grille[idx_1][0];
            if joueur != 0 {
                let mut temp = true;
                for idx_2 in 1..3 {
                    temp &= self.grille[idx_1][idx_2] == joueur;
                }
                if temp {
                    return Some(joueur);
                }
            }
            // Teste des colonnes
            let joueur = self.grille[0][idx_1];
            if joueur != 0 {
                let mut temp = true;
                for idx_2 in 1..3 {
                    temp &= self.grille[idx_2][idx_1] == joueur;
                }
                if temp {
                    return Some(joueur);
                }
            }
        }
        let joueur_diag = self.grille[0][0];
        let joueur_antidiag = self.grille[2][0];
        let mut temp_diag = joueur_diag != 0;
        let mut temp_antidiag = joueur_antidiag != 0;
        for idx in 1..3 {
            temp_diag &= self.grille[idx][idx] == joueur_diag;
            temp_antidiag &= self.grille[2 - idx][idx] == joueur_antidiag;
        }
        if temp_diag {
            return Some(joueur_diag);
        }
        if temp_antidiag {
            return Some(joueur_antidiag);
        }
        if self.tour >= 9 {
            return Some(0);
        }
        None
    }
}

impl PartialEq for Partie {
    fn eq(&self, other: &Self) -> bool {
        let mut val = true;
        for idx_ligne in 0..3 {
            for idx_colonne in 0..3 {
                val &= self.grille[idx_ligne][idx_colonne] == other.grille[idx_ligne][idx_colonne];
            }
        }
        val
    }
}

#[test]
/// Test pour vérifer l'initialisation
fn initialisation() {
    assert_eq!(
        Partie::new(),
        Partie {
            grille: [[0, 0, 0], [0, 0, 0], [0, 0, 0]],
            tour: 0
        }
    );
}

#[test]
/// Test de représentation vide
fn representation_vide() {
    let partie = Partie::new();
    let representation = partie.to_string();
    assert_eq!(
        representation,
        " | | 
-+-+-
 | | 
-+-+-
 | | 
"
    );
}

#[test]
/// Test de représentation joué
fn representation_joue() {
    let mut partie = Partie::new();
    partie.jouer(0, 0).unwrap();
    partie.jouer(0, 1).unwrap();
    partie.jouer(0, 2).unwrap();
    partie.jouer(1, 0).unwrap();
    partie.jouer(1, 2).unwrap();
    let representation = partie.to_string();
    assert_eq!(
        representation,
        "X|O|X
-+-+-
O| |X
-+-+-
 | | 
"
    );
}

#[test]
fn double_jeu_case() {
    let mut partie = Partie::new();
    partie.jouer(1, 1).unwrap();
    let val = match partie.jouer(1, 1) {
        Err(e) => e,
        Ok(_) => panic!(),
    };
    assert_eq!(val, 1);
}

#[test]
fn victoire_horizontal() {
    let mut partie = Partie::new();
    let coups = [[1, 0], [2, 0], [1, 1], [2, 2], [1, 2]];
    for coup in coups {
        assert_eq!(partie.victoire(), None);
        partie.jouer(coup[0], coup[1]).expect("Coup impossible");
    }
    assert_eq!(partie.victoire(), Some(1));
}

#[test]
fn victoire_vertical() {
    let mut partie = Partie::new();
    let coups = [[1, 0], [2, 1], [0, 0], [2, 2], [2, 0]];
    for coup in coups {
        assert_eq!(partie.victoire(), None);
        partie.jouer(coup[0], coup[1]).expect("Coup impossible");
    }
    assert_eq!(partie.victoire(), Some(1));
}

#[test]
fn victoire_diagonal() {
    let mut partie = Partie::new();
    let coups = [[0, 0], [2, 0], [1, 1], [2, 1], [2, 2]];
    for coup in coups {
        assert_eq!(partie.victoire(), None);
        partie.jouer(coup[0], coup[1]).expect("Coup impossible");
    }
    assert_eq!(partie.victoire(), Some(1));
}
