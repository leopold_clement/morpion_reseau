extern crate argparse;
use argparse::{ArgumentParser, Store, StoreFalse, StoreTrue};

use std::io;
use std::io::{Read, Write};

use std::net::TcpStream;

use crate::jeu::*;
mod jeu;

use crate::multi::*;
mod multi;

/// Point d'entré du programme
fn main() {
    let mut addr = String::from("127.0.0.1:8788");
    let mut est_serveur = true;

    {
        let mut ap = ArgumentParser::new();
        ap.set_description("Jeu de morpion de Léopold, trés trés bon.");
        ap.refer(&mut est_serveur)
            .add_option(&["-s", "--serveur"], StoreTrue, "Etre serveur")
            .add_option(&["-c", "--client"], StoreFalse, "Etre le client");
        ap.refer(&mut addr).add_option(
            &["--addr"],
            Store,
            "Adresse à laquelle se connecter pour le client",
        );
        ap.parse_args_or_exit();
    }

    let stream = if est_serveur {
        attente_partenaire()
    } else {
        connexion_partenaire(addr)
    };
    main_partie(stream, est_serveur);
}

/// Algorithme de gestion du jeu
fn main_partie(mut stream: TcpStream, est_joueur_1: bool) {
    let mut partie = Partie::new();
    let joueur_local = if est_joueur_1 { 1 } else { 2 };
    println!("{}", partie.to_string());
    loop {
        if joueur_local == partie.joueur_a_jouer() {
            println!("A toi de joué");
            loop {
                println!("Ligne :");
                let ligne = lire_chiffre();
                println!("Colonne :");
                let colonne = lire_chiffre();

                if (ligne > 2) | (colonne > 2) {
                    println!("Valeur invalide");
                    continue;
                }

                match partie.jouer(ligne.into(), colonne.into()) {
                    Ok(_) => {
                        envoie_coup(&mut stream, ligne, colonne);
                        break;
                    }
                    Err(joueur) => println!("Le joueur {} a déjà joué ici", joueur),
                }
            }
        } else {
            let coup = recevoire_coup(&mut stream);
            match partie.jouer(coup[0].into(), coup[1].into()) {
                Err(_) => panic!("Coup recu invalide"),
                Ok(_) => {}
            }
        }
        println!("{}", partie.to_string());
        match partie.victoire() {
            Some(joueur) => {
                if joueur_local == joueur {
                    println!("Tu as gagné");
                } else {
                    println!("Tu as perdu");
                }
                return;
            }
            None => {}
        }
    }
}

/// Lis et convertie une entrée de la console en ```u8```
fn lire_chiffre() -> u8 {
    let mut buffer = String::new();
    io::stdin()
        .read_line(&mut buffer)
        .expect("Erreur de lecture dans la console");
    let trimmed = buffer.trim();
    trimmed.parse::<u8>().expect("Erreur de conversion")
}

/// Envoie un coup à l'autre joueur
fn envoie_coup(stream: &mut TcpStream, ligne: u8, colonne: u8) {
    let buffer = [ligne, colonne];
    stream.write(&buffer).expect("Impossible d'envoyer le coup");
    stream.flush().unwrap();
}

/// Recoie le coup de l'autre joueur
fn recevoire_coup(stream: &mut TcpStream) -> [u8; 2] {
    let mut buffer = [0; 2];
    stream
        .read(&mut buffer)
        .expect("Impossible de recevoir le coup");
    return buffer;
}
