//! Mécanique de connexion entre deux joueur

use std::net::{TcpListener, TcpStream};

/// Ouvre en socket en renvoie le premier client connecté
///
/// Le port utilisé est le 8788
pub fn attente_partenaire() -> TcpStream {
    let socket = TcpListener::bind("0.0.0.0:8788").expect("Ouverture du socket impossible");
    let tuple = socket.accept().unwrap();
    tuple.0
}

/// Se connecte à un port TCP
///
/// # Argument
/// * ```addr``` : une chaine représentant l'adresse et le port cible
pub fn connexion_partenaire(addr: String) -> TcpStream {
    return TcpStream::connect(addr).expect("Impossible de se connecté");
}
